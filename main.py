"""
Zadanie 2

Napisać program rozwiązujący problem komiwojażera
(minimalizacja drogi pomiędzy n miastami bez powtórzeń)
przy pomocy algorytmu genetycznego.

Zastosować reprodukcję przy użyciu
wy - nieproporcjonalnej ruletki,
D - operator krzyżowania CX, https://www.youtube.com/watch?v=85pIA2TYsUs
D - oraz mutację równomierną.

Program powinien umożliwiać użycie
D - różnych wielkości populacji,
D - liczby iteracji,
D - prawdopodobieństwa mutacji.

Program powinien zapewnić wizualizację wyników w postaci
 - wykresów średniego przystosowania
 - maksymalnego przystosowania
 - minimalnego przystosowania (długości trasy) dla kolejnych populacji
P - 2 map (o wymiarach 10x10 punktów),
   na których będą wyświetlane miasta oraz drogi najdłuższa i najkrótsza.

Pokazać działanie programu na danych testowych składających się z 10 miast,
opisanych za pomocą współrzędnych na mapie o wymiarach 10x10 punktów.
bane testowe: miasta:
A(4,4), B(1,1), C(8,9), D(2,10), E(4,10), F(6,9), G(5,6), H(1, 8), 1(8,7), J(9,4)
"""

from random import randint
from statsmodels.compat import scipy
from math import sqrt
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import numpy as np

CITIES = 10
POPULATION_AMOUNT = 5
ITERATIONS = 25
MUTATION_PROBABILITY = 0.2

# x = [4,1,8,2,4,6,5,1,8,9]
# y = [4,1,9,10,10,9,6,8,7,4]

x = [4,1,8,2,4,6,5,1,8,9]
y = [4,1,9,10,10,9,6,8,7,4]

##################################
############ WYKRES  #############
##################################
def wykres():
    # plot_coords = [0,11,0,11]

    # plt.plot([x[6],x[7]],[y[6], y[7]])
    # plt.plot([x[3],x[7]],[y[3], y[7]])
    # add_connection(8,3)
    # plt.plot(conn)
    plt.scatter(x, y)
    plt.xticks(np.arange(0,11,1.0))
    plt.yticks(np.arange(0,11,1.0))
    plt.grid(True)
    plt.show()

def show_fenotyp(type, fenotyp):
    dlugosc = len(fenotyp) - 1
    for index in range(0,dlugosc):
        city1x = x[int(fenotyp[index])]
        city1y = y[int(fenotyp[index])]
        city2x = x[int(fenotyp[index + 1])]
        city2y = y[int(fenotyp[index + 1])]
        plt.plot([city1x, city2x], [city1y, city2y])
    city11x = x[int(fenotyp[dlugosc])]
    city11y = y[int(fenotyp[dlugosc])]
    city22x = x[int(fenotyp[0])]
    city22y = y[int(fenotyp[0])]
    plt.plot([city11x, city22x], [city11y, city22y])
    plt.scatter(x, y)
    plt.xticks(np.arange(0,11,1.0))
    plt.yticks(np.arange(0,11,1.0))
    plt.title(type)
    plt.grid(True)
    plt.show()

def show_min(lista):
    plt.xticks(np.arange(0, len(lista)+1, 1))
    for i in range(len(lista)-1):
        plt.plot([i+1,i+2],[lista[i][1],lista[i+1][1]])
    plt.title('Minimalne przystosowanie')
    plt.grid(True)
    plt.show()

def show_max(lista):
    plt.xticks(np.arange(0, len(lista)+1, 1))
    for i in range(len(lista)-1):
        plt.plot([i+1,i+2],[lista[i][1],lista[i+1][1]])
    plt.title('Maksymalne przystosowanie')
    plt.grid(True)
    plt.show()

def show_avg(lista):
    plt.xticks(np.arange(0, len(lista)+1, 1))
    for i in range(len(lista)-1):
        plt.plot([i+1,i+2],[lista[i],lista[i+1]])
    plt.title('Średnie przystosowanie')
    plt.grid(True)
    plt.show()

    pass

def add_connection(point1, point2):
    return plt.plot(
        [x[point1], x[point2]],
        [y[point1], y[point2]]
    )

##################################
############ VALIDACJA ###########
##################################

def is_fenotyp_valid(fenotyp):
    if len(fenotyp) != 10:
        return False

    dict_wystapienie = {
        '0': 0,
        '1': 0,
        '2': 0,
        '3': 0,
        '4': 0,
        '5': 0,
        '6': 0,
        '7': 0,
        '8': 0,
        '9': 0,
    }
    for city in fenotyp:
        dict_wystapienie[city] +=1

    #każde miasto powinno wystapic TYLKO JEDEN RAZ
    for city in fenotyp:
        if dict_wystapienie[city] != 1:
            return False
    return True

##################################
############ RATINGS DINSTANCE #############
##################################
def rate_fenotyp(fenotyp):
    distance = 0
    # dlugosc = None
    # try:
    dlugosc = len(fenotyp)
    # except Exception as e:
    #     print("cos nie tak: {} ||| {}".format(dlugosc, e))
    for index in range(0,dlugosc-1): #wszystkie licz tak samo
        city1x = x[int(fenotyp[index])]
        city1y = y[int(fenotyp[index])]
        city2x = x[int(fenotyp[index+1])]
        city2y = y[int(fenotyp[index+1])]
        distance += get_distance(city1x,city1y,city2x,city2y)

    #ostatnia odleglosc to dystans 10->1 więc nie chcemy błędów
    city11x = x[int(fenotyp[len(fenotyp)-1])]
    city11y = y[int(fenotyp[len(fenotyp)-1])]
    city22x = x[int(fenotyp[0])]
    city22y = y[int(fenotyp[0])]
    distance += get_distance(city11x, city11y, city22x, city22y)
    return distance

def get_distance(x1,y1,x2,y2):
    # city2 = int(_city2)
    # x1 = x[city1]
    # y1 = y[city1]
    # x2 = x[city2]
    # y2 = y[city2]
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def get_best(lista):
    best_val = 999999
    for fenotyp in lista:
        if float(rate_fenotyp(fenotyp))<float(best_val):
            best_fenotyp = fenotyp
            best_val = rate_fenotyp(fenotyp)
    return best_fenotyp, best_val

def get_avg(lista):
    suma = 0
    for fenotyp in lista:
        suma += rate_fenotyp(fenotyp)
    return suma/len(lista)

def get_worst(lista):
    worst_val = 0
    for fenotyp in lista:
        if float(rate_fenotyp(fenotyp))>float(worst_val):
            worst_fenotyp = fenotyp
            worst_val = rate_fenotyp(fenotyp)
    return worst_fenotyp, worst_val

#############################################
#################SELECTION RULETKA ################
#############################################
def ruletka_selection(population):
    val_sum = {}
    val_dict = {}
    wheel_list = []
    wheel_result = {}

    # DODAJ WSZYSTKIE CHROMOSOMY DO LISTY
    for val in val_dict:#lista chromosomow
        for i in range(val_dict[val]):#tyle ile chromosom ma wartość (np 5/20) tyle razy (5) zapisujemy do listy
            wheel_list.append(val)

    # PRZYGOTUJ DICTA
    for chrm in val_dict:
        wheel_result[chrm] = 0

    # ZAKREC N RAZY KOLEM I WYLOSUJ INDEXY
    wheel_size = len(wheel_list)
    for i in range(wheel_size):# zakrec tyle razy ile miejsc
        random_index = randint(0,wheel_size-1)
        # bit_index = self.get_bin(random_index)
        wheel_result[wheel_list[random_index]] +=1
        # wheel_result[bit_index] += 1

    # OKRESL CZY PRZETRWAL CZY NIE
    chromosomes_result = {}
    for index in wheel_result:
        if wheel_result[index] > 0:
            chromosomes_result[index] = True
        else:
            chromosomes_result[index] = False

    survived_chromosomes = []
    for chrm in chromosomes_result:
        if chromosomes_result[chrm] == True:
            survived_chromosomes.append(chrm)

    return population


##################################
############ CROSS #############
##################################

def cross_population(population):
    quantity = len(population)
    new_cities = []
    while(quantity>0):
        fenotyp1 = population[quantity-1]
        fenotyp2 = population[quantity-2]
        quantity -=2
        child = cross_fenotyp(fenotyp1, fenotyp2)
        new_cities.append(child)
    return new_cities

def cross_fenotyp(fenotyp1, fenotyp2):
    is_loop_run = True
    current_indeks = 0
    cycle = fenotyp1[current_indeks] # '0'
    while(is_loop_run):
        current_indeks = int(cycle[len(cycle)-1]) #4
        cycle += fenotyp2[current_indeks] #'041'
        #koniec cyklu
        if fenotyp2[current_indeks] == fenotyp1[0]:
            is_loop_run = False

    child = list(CITIES*'x')
    for val in cycle:
        index_val = int(val) #int bo wartosc bedzie indeksem
        child[index_val] = str(index_val)

    for index, value in enumerate(child):
        if value == 'x':
            child[index] = fenotyp2[index]
    child_gooood = ''
    for litera in child:
        child_gooood += litera
    return child_gooood

##################################
############ MUTATIOn #############
##################################

def mutate_population(population):
    population_copy = population[:]
    for index, value in enumerate(population):
        if if_mutate():
            population_copy[index] = mutate(value)
    return population_copy

def mutate(_fenotyp):
    fenotyp = []
    [fenotyp.append(i) for i in _fenotyp]
    first= randint(0,len(fenotyp)-1)
    second = first
    while(second==first):
        second = randint(0,len(fenotyp)-1)
    value_first = fenotyp[first]
    value_second = fenotyp[second]
    # value_first, value_second = value_second, value_first
    new_fenotyp = fenotyp
    new_fenotyp[first] = value_second
    new_fenotyp[second] = value_first

    fenotyp_dozwrocenia = ''
    for litera in new_fenotyp:
        fenotyp_dozwrocenia += litera
    return fenotyp_dozwrocenia

def if_mutate():
    random_val = randint(1,100)
    max_range = MUTATION_PROBABILITY*100
    if random_val>=1 and random_val<=max_range: #np. MP = 0,2 => max range = 20 => prawdopodobienstwo, ze random_val>=1 i random_val<=20 => 20/100 => 0,2
        return True
    return False

##################################
############ get random fenotyp #############
##################################
def create_fenotyp():
    fenotyp =''
    while(not is_fenotyp_valid(fenotyp)):
        fenotyp = str(randint(0, 9999999999))
    return fenotyp

def main():

    population = []

    #startowa populacja
    for i in range(0, POPULATION_AMOUNT):
        population.append(create_fenotyp())

    bests = []
    worsts = []
    avgs = []
    for i in range(0, ITERATIONS):
        population = population + cross_population(population)
        population = mutate_population(population)
        population = ruletka_selection(population)
        bests.append(get_best(population))
        avgs.append(get_avg(population))
        worsts.append(get_worst(population))

    worst_f, worst_v = get_worst(population)
    best_f, best_v = get_best(population)

    print("worst fenotyp: {}".format(worst_f))
    print("worst value: {}".format(worst_v))
    print("best fenotyp : {}".format(best_f))
    print("best value : {}".format(best_v))
    print(worsts)
    print(bests)
    print(avgs)
    show_fenotyp('Best', best_f)
    show_fenotyp('Worst', worst_f)
    show_min(worsts)
    show_max(bests)
    show_avg(avgs)
main()

